#include <stdio.h>
int main() {
    char string[1000], i;
    int j = 0;

    printf("Enter a string: ");
    fgets(string, sizeof(string), stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &i);

    for (int k = 0; string[k] != '\0'; ++k) {
        if (i == string[k])
            ++j;
    }

    printf("Frequency of %c = %d", i, j);
    return 0;
}