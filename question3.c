#include <stdio.h>

int main()
{
  int m, n, p, q, c, d, k,i,j, total = 0;
  int first[100][100], second[100][100], multiply[100][100],sum[100][100];

  printf("Enter number of rows and columns of first matrix\n");
  scanf("%d%d", &m, &n);
  printf("Enter elements of first matrix\n");

  for (c = 0; c < m; c++)
    for (d = 0; d < n; d++)
      scanf("%d", &first[c][d]);

  printf("Enter number of rows and columns of second matrix\n");
  scanf("%d%d", &p, &q);


  {
    printf("Enter elements of second matrix\n");

  if (n != p)
    printf("The multiplication isn't possible.\n");
  else{
    for (c = 0; c < p; c++)
      for (d = 0; d < q; d++)
        scanf("%d", &second[c][d]);

    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++) {
        for (k = 0; k < p; k++) {
          total = total + first[c][k]*second[k][d];
        }

        multiply[c][d] = total;
        total = 0;
      }
    }

    printf("Product of the matrices:\n");

    for (c = 0; c < m; c++) {
      for (d = 0; d < q; d++)
        printf("%d\t", multiply[c][d]);

      printf("\n");
    }}
  if (m!=p||n!=q)
    printf("The adding isn't possible.\n");
  else{
  for (i = 0; i < m; ++i)
        for (j = 0; j < n; ++j) {
            sum[i][j] = first[i][j] + second[i][j];
        }
printf("\nSum of two matrices: \n");
    for (i = 0; i < m; ++i)
        for (j = 0; j < n; ++j) {
            printf("%d   ", sum[i][j]);
            if (j == n - 1) {
                printf("\n\n");
            } }}
  }
  return 0;
}
